const express = require('express');
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');
const app = express();
const port = 3001;

//Mongodb atlas
//Connect to the database by passing in your connection string, remember to replace <password> and database name with actual values.
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.yh3uh.mongodb.net/batch127_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
//Connecting to Robo3t locally
/*
mongoose.connect("mongodb://localhost:27017/database name", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
*/
//set notifications for connection success or failure
//Connection to the databes, allows us to handle errors when the initial connection is established.
let db = mongoose.connection;
//If a connection error occured, output in the console
//console.error.bind(console) = allows us to print error in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"))
//if the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"))

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Mongoose Schemas

//Schemas determine the structure of the documents to be written in the database.
//Schemas act as blueprints to out data
//Use the Schema() constructor of the Mongoose module to create a new Schema object
//The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	//Define the fields with the corresponding datatype
	//for task, it needs a "task name" and "task status"
	name: String,
	status: {
		type: String,
		//Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
})
//Task is capitalized following the MVC approach for naming conventions
//Model-View-Controller
const Task = mongoose.model("Task", taskSchema)
//The first parameter of the Mongoose model method indicates the collection in where to store the data
//The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection



//Create a new task
/*
1. Add a functionality to check if there are duplicate tasks
	-if the task already exists in the database, we return an error
	-if the task doesn't exist in  the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not neeed to be provided because our schema default is to "pending" upon creation of an object
*/
app.post('/tasks', (req,res)=>{
	//check if there are duplicate tasks
	//findOne is a mongoose method that acts similar to "find" of mongoDB
	//findOne() returns the first document that matches the search criteria
	Task.findOne({ name:req.body.name }, (err,result)=>{
		//if a document was found and the document's name matches the information sent via the client/postman
		if (result !== null && result.name == req.body.name){
			//return a message to the client
			return res.send("Duplicate task found")
		}else{
			//if no document was found, create a new task and save it to our database
			let newTask = new Task({
				name: req.body.name
			})
			
			newTask.save((saveErr, saveTask)=>{
				if (saveErr) {
					return console.error(saveErr)
				}else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})


//Getting all the tasks

/*
Business Logic
1. We will retrieve all the documents using the get method
2. If an error is encountered, print the error
3. If no errors are found send a success status back to the client/postman and return an array of document
*/

app.get('/tasks',(req,res)=>{
	//"find" is a mongoose method is similar to MongoDB "find". Empty string {} means it returns all the documents and stores them in the result parameter
	Task.find({},(err,result)=>{
		//if error occured
		if(err){
			return console.log(err)
		}else{
			//if no errors are found
			//status 200 means that everything is OK in terms of processing
			//the "json" method allows to send a JSON format for the response
			//The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				data: result
			})
		}
	})
})


//s30 Activity

/*Business Logic for POST /signup
1. Add a functionality to check if there are duplicate tasks
-If the user already exists in the database, we return an error
-If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new user object with a "username" and "password" fields/properties
Business Logic for GET /users
1. We will retrive all the documents using the get method
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/postman and return an array of document*/


const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User",userSchema)

app.post('/signup',(req,res)=>{
	User.findOne({username: req.body.username},(err,result)=>{
		if (result !== null && result.username == req.body.username) {
			return res.send("This username has already been taken.")
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, saveUser)=>{
				if (saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(200).send(`${req.body.username} has been registered successfully`)
				}
			})
		}
	})
})

app.get('/users',(req,res)=>{
	User.find({},(err,result)=>{
		if(err){
			console.log(err)
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})

app.listen(port, ()=> console.log(`Server is running at port ${port}`))